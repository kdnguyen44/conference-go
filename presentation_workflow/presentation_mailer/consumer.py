import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


# def send_reject_email(request, id):
#     presentation = Presentation.objects.get(id=id)
#     subject = "Your presentation has been rejected"
#     email_body = f"{presentation.presenter_name}, we're sad to tell you that your presentation {presentation.title} has been rejected"
#     send_mail(
#         subject,
#         email_body,
#         "admin@conference.go",
#         presentation.presenter_email,
#     )

while True:
    try:

        def process_approval(ch, method, properties, body):
            approve_body = json.loads(body)
            name = approve_body.get("presenter_name")
            title = approve_body.get("title")
            subject = "Your presentation has been accepted"
            email_body = (
                name
                + ", we're happy to tell you that your presentation "
                + title
                + " has been accepted"
            )
            send_mail(
                subject,
                email_body,
                "admin@conference.go",
                [approve_body.get("presenter_email")],
            )

        def process_rejection(ch, method, properties, body):
            reject_body = json.loads(body)
            name = reject_body.get("presenter_name")
            title = reject_body.get("title")
            subject = "Your presentation has been rejected"
            email_body = (
                name
                + ", we're sad to tell you that your presentation "
                + title
                + " has been rejected"
            )
            send_mail(
                subject,
                email_body,
                "admin@conference.go",
                [reject_body.get("presenter_email")],
            )

        def main():
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue="presentation_approvals")
            channel.queue_declare(queue="presentation_rejections")
            channel.basic_consume(
                queue="presentation_approvals",
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.basic_consume(
                queue="presentation_rejections",
                on_message_callback=process_rejection,
                auto_ack=True,
            )
            channel.start_consuming()

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
