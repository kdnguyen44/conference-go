from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
from django.http import JsonResponse
import json


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    search = {
        "query": [city, state],
        "per_page": 1,
    }
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=search, headers=headers)
    dict = json.loads(response.content)
    return dict["photos"][0]["url"]


# def get_weather_data(city, state):
#     headers = {"Authorization": OPEN_WEATHER_API_KEY}
#     response = requests.get(url, headers=headers)
