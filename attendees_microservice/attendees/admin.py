from django.contrib import admin

from .models import Attendee, Badge, AccountVO, ConferenceVO


@admin.register(Attendee)
class AttendeeAdmin(admin.ModelAdmin):
    pass


@admin.register(Badge)
class BadgeAdmin(admin.ModelAdmin):
    pass


@admin.register(AccountVO)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(ConferenceVO)
class ConferenceAdmin(admin.ModelAdmin):
    pass
